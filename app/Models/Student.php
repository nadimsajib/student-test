<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class Student extends Model
{
    use HasFactory;
    protected $table = 'student_info';
    protected $fillable = ['std_id','name','address','sex','dob','age'];

    public function getAllSp(){
        $student = DB::select('call GetStudents()');
        return $student;
    }
}
