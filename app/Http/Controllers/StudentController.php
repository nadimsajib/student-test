<?php
namespace App\Http\Controllers;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\StudentPostRequest;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$students = Student::all(['id','std_id','name']);
        $students = Student::getAllSp();
        //print_r($students);exit;
        return response()->json($students);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentPostRequest $request)
    {
        //print_r($request->post());exit;
        $student = Student::create($request->post());
        return response()->json([
            'message' => 'Student Created Successfully!!!',
            'student' => $student
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return response()->json($student);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $student->fill($request->post())->save();

        return response()->json([
            'message' => 'Student Updated Successfully!!!',
            'student' => $student
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return response()->json([
            'message' => 'Student Deleted Successfully!!!'
        ]);
    }
}